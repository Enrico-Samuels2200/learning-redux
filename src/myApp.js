import React from "react";
import InputText from "./components/InputText";
import { connect } from "react-redux";
import "./index.css";

class MyApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onTextChange = () => {
    const { inputText } = this.props;
  };

  render() {
    const { inputText } = this.props;
    return (
      <div className="myApp" style={{ textlign: "center" }}>
        <h1>{inputText}</h1>
        <InputText />
      </div>
    );
  }
}

export default connect(
  state => ({
    ...state.App,
    inputText: state.TextProps.text
  }),
  {}
)(MyApp);
