import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import store from "./redux/store";

import MyApp from "./myApp";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <MyApp />
  </Provider>,
  rootElement
);
