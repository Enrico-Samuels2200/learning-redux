import React from "react";
import { connect } from "react-redux";
import textActions from "../redux/actions";

const { textChange, textSizeChange } = textActions;

class InputText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onTextChange = e => {
    const { text, textChange, textSize } = this.props;
    const value = e.target.value;
    console.log("value", value);
    textChange(value);
  };

  changeSize = () => {
    const { textSizeChange } = this.props;
    textSizeChange("20px");
  };

  changeTextSizeBack = () => {
    const { textSizeChange } = this.props;
    textSizeChange("14px");
  };

  render() {
    const { text, textSize } = this.props;
    return (
      <div>
        <input
          style={{ fontSize: textSize }}
          onChange={this.onTextChange}
          value={text}
        />
        <br />
        <br />
        <button onClick={this.changeSize}>Change Text Size to 20px</button>
        <button onClick={this.changeTextSizeBack}>
          Change Text Size to 14px
        </button>
      </div>
    );
  }
}

export default connect(
  state => ({
    ...state.App,
    text: state.TextProps.text,
    textSize: state.TextProps.textSize
  }),
  { textChange, textSizeChange }
)(InputText);
