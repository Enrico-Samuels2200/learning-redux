const actions = {
  TEXT_CHANGE: "TEXT_CHANGE",
  TEXT_SIZE_CHANGE: "TEXT_SIZE_CHANGE",

  textChange: text => ({
    type: actions.TEXT_CHANGE,
    payload: text
  }),
  textSizeChange: textSize => ({
    type: actions.TEXT_SIZE_CHANGE,
    payload: textSize
  })
};

export default actions;
